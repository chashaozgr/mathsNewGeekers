# coding:utf-8
'''
author:wangyi
'''

num = [str(i) for i in range(10)]
alpha = [chr(i) for i in range(97, 123)]


def get_char_list(string):

    num_char = ''
    alpha_char = ''
    char_list = []
    for i in range(len(string)):
        if string[i] in num:
            if len(alpha_char) > 0:
                char_list.append(alpha_char)
                alpha_char = ''
            num_char += string[i]
        else:
            if string[i].lower() in alpha:
                if len(num_char) > 0:
                    char_list.append(num_char)
                    num_char = ''
                alpha_char += string[i]
            else:
                if len(num_char) > 0:
                    char_list.append(num_char)
                    num_char = ''
                if len(alpha_char) > 0:
                    char_list.append(alpha_char)
                    alpha_char = ''
                char_list.append(string[i])
    if len(num_char) > 0:
        char_list.append(num_char)
    if len(alpha_char) > 0:
        char_list.append(alpha_char)
    return char_list



if __name__ == '__main__':


    string = '315N晚b会'
    get_char_list(string.decode('utf-8'))