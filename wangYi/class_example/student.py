from person import Person

class Student(Person):

    def __init__(self,gender,age,grade):
        super(Student,self).__init__(gender,age)
        self.grade = grade

    def update_grade(self,grade):
        self.grade = grade


if __name__ == '__main__':
    student = Student('man', 18, 60)
    print(student.gender, student.age, student.grade)
    student.update_grade(57)
    print(student.grade)
    student.update_age()
    print(student.age)