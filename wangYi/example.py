# coding:utf-8
'''
author:wangyi
'''

class Person:

    def __init__(self,gender,age): # self.__init__(gender)

        self.gender = gender
        self.age = age

    def update_age(self):

        self.age += 1

class Student(Person):

    def __init__(self,gender,age,grade):
        super(Student,self).__init__(gender,age)
        self.grade = grade

    def update_grade(self,grade):
        self.grade = grade



if __name__ == '__main__':

    a = 1
    person = Person('man',25)
    person.update_age()
    print(person.gender)
    print(person.age)
    student = Student('man',18,60)
    print(student.gender,student.age,student.grade)
    student.update_grade(57)
    print(student.grade)
    student.update_age()
    print(student.age)

