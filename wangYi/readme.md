# how to use word2vec_practice project

- 语料来自搜狗新闻语料,原始格式为dat,已经转化为带标签的txt,需要分词进一步转化成训练格式,下列步骤已完成

## 1.处理原始语料为word2vec训练格式

- 将[corpus.txt](https://pan.baidu.com/s/16Vr_1izOOEooQvMm59tL0Q)(提取码：3tag)文件放到w2v_practice.py同级目录下,运行程序中parse_xml_to_corpus方法,即可得到可用来训练格式的语料

## 2.训练w2v词向量

-  将[corpus_train.txt](https://pan.baidu.com/s/1BUePyJ1LJw90eZ6w93N3Xw)(提取码：deco)文件放到w2v_practice.py同级目录下,运行程序中train_w2v方法,即可开始w2v训练,训练之前可根据自己的喜好调整参数

## 3.根据训练完成的词向量获取前topN的相近词

- 将[w2v.txt](链接：https://pan.baidu.com/s/1YUaK_X68isvTcnWbQAaJBw)(提取码：84ef)文件放到w2v_practice.py同级目录下,运行程序中get_similar_words方法,即可开始测试,读取模型时间大概有4分钟左右,请耐心等待

- 两个例子:

![相似词汇](https://images.gitee.com/uploads/images/2018/1221/151657_8048d86d_847413.png "similar_test.png")
![相似词汇](https://images.gitee.com/uploads/images/2018/1221/151731_e1fe0c84_847413.png "similar_test_2.png")
## 以上每一步都提供已经完成的相应结果,可选择其中一步来尝试,也可以根据自己的时间和兴趣把1-3整个流程走一遍