# -*- coding:utf8 -*-
from wxpy import *

'''
微信机器人登录有3种模式，
(1)极简模式:robot = Bot()
(2)终端模式:robot = Bot(console_qr=True)
(3)缓存模式(可保持登录状态):robot = Bot(cache_path=True)
'''
#初始化机器人，选择缓存模式（扫码）登录
robot = Bot(cache_path=True)

#获取好友、群、公众号信息
robot.chats()
#获取好友的统计信息
Friends = robot.friends()
# print Friends
print(Friends.stats_text())

# 下面是读到数据库
    # conn = MySQLdb.connect(host='127.0.0.1', user='root', passwd='', charset="utf8", use_unicode=False)  # 连接服务器
    # cur = conn.cursor()
    # sql = "insert into weixin.head_img(NickName,head_img) values(%s,%s)"
    # param = (NickName, Sex, Province, City, Signature)

    # 这是放到桌面的文件夹里面
    # fileImage = open('C:\Users\pc\Desktop\weixin_image' + "/" + str(i) + ".jpg", 'wb')
    # fileImage.write(img)
    # fileImage.close()