<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" 
    import="com.gy.sams.entity.*,com.gy.sams.dao.*,com.gy.sams.dao.impl.*,java.util.List,java.util.ArrayList,java.*" 
    contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	User user=null;
	if(session.getAttribute("user")!=null){
		user=(User)session.getAttribute("user");
	}else{
		out.print("<script type='text/javascript'>alert('您没有登陆！');location.replace('index.jsp')</script>");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>成绩查询</title>
<script type="text/javascript">
function poForm()
{
	var name=document.form.Name;
	var grade=document.form.Grade;
	if(name==""||name==" "){
		alert("姓名获取失败！");
		return false;
	}else if(grade==""||grade==" "){
		alert("分数获取失败！");
		return false;
	}
	return true;
}
</script> 
<style type="text/css"> 
.info a{ text-decoration:none; color:#F93}
.info a:hover{ color:#F00;font-weight:bold} 
.info{ float:right;} 
.main{clear:both; }
.main h1{ margin:0; padding:0px; line-height:80px;}
.list{ border:#099 solid 2px; background:#EEFAD3; width:160px; height:30px; margin-top:5px;} 
.list div{width:500px; float:left; margin-left:10px; margin-top:5px;overflow:hidden;text-overflow:ellipsis; white-space:nowrap;} 
</style>



</head>
<body>
	<div style="margin:0 auto;width:700px;">
		<div style="border: 2px #0C9 solid; width:700px; height:300px; margin-top:150px; background:#DFEDB4">
			<h1 style="width:160px;margin:0 auto">成绩查询</h1>
			<div style="width:595px;margin:0 auto">
			
				<form name="form" method="post" action="admin/doDelete.jsp" onsubmit="return poForm()" style="margin:0;padding:0">
					<table width="600px" border="1">
						<tr>
							<td>姓名：</td>
							<td>分数：</td>
							<td>操作：</td>
						</tr>
						<%
							IScoreDao scoreDao=new ScoreDaoImpl();
							Score score=new Score();
							List<Score> list=new ArrayList<Score>();
							list=scoreDao.getAllScores();
							for(int i=0;i<list.size();i++){
								score=list.get(i);			
						%>	
						<tr>
							<td><label><input type="text" name="Name" value="<%=score.getId()%>"/></label></td>
							<td><label><input type="text" name="Name" value="<%=score.getSname()%>"/></label></td>
							<td><label><input type="text" name="Grade" value="<%=score.getSscore()%>"/></label></td>
							<td style="text-align:center">
								<label><input type="submit" name="button1" id="button1" value="删除" /></label>
								<!-- <label><input type="submit" name="button2" id="button2" value="修改"/></label>
								<label><input type="submit" name="button3" id="button3" value="增加"/></label>	 -->					
							</td>
						</tr>
						<%} %>
					</table>
				</form>
			</div>
		</div>
	</div>
</body>
</html>