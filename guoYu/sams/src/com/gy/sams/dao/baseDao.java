package com.gy.sams.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class baseDao {
	public static final String driver="com.mysql.jdbc.Driver";
	public static final String url="jdbc:mysql://127.0.0.1:3306/sams";
	public static final String user="root";
	public static final String password="2018kycg";
	
	/*
	 * 连接数据库，
	 */
	public Connection getConn() {
		/**
		 * 1.定义变量
		 * 2.连接，抛出异常
		 * 3.返回连接
		 */
		//1.定义变量
		Connection conn=null;
		//2.连接，抛出异常
		try {
			Class.forName(driver);
			conn=DriverManager.getConnection(url,user,password);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		//3.返回连接
		return conn;
	}
	/*
	 * 分别关闭conn,pstmt,rs
	 */
	public void closeAll(Connection conn,PreparedStatement pstmt,ResultSet rs) {
		/*
		 * 1.关闭conn
		 * 2.关闭pstmt
		 * 3.关闭rs
		 * */
		//1.关闭conn
		if(conn!=null) {
			try {
				conn.close();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		//2.关闭pstmt
		if(pstmt!=null) {
			try {
				pstmt.close();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
		//3.关闭rs
		if(rs!=null) {
			try {
				rs.close();
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
