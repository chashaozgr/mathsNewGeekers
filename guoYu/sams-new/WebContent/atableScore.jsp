<?xml version="1.0" encoding="UTF-8" ?>
<!-- <%@page import="java.util.ArrayList"%> -->
<%@ page language="java" 
    import="com.gy.sams.entity.*,com.gy.sams.dao.*,com.gy.sams.dao.impl.*,java.util.List,java.util.ArrayList" 
    contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	User user=null;
	if(session.getAttribute("user")!=null){
		user=(User)session.getAttribute("user");
	}else{
		out.print("<script type='text/javascript'>alert('您没有登陆！');location.replace('index.jsp')</script>");
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>成绩查询</title>
<style type="text/css"> 
.info a{ text-decoration:none; color:#F93}
.info a:hover{ color:#F00;font-weight:bold}
.info{ float:right;}
.main{clear:both; }
.main h1{ margin:0; padding:0px; line-height:80px;}
.list{ border:#099 solid 2px; background:#EEFAD3; width:180px; height:30px; margin-top:5px;}
.list div{width:180px; float:left; margin-left:10px; margin-top:5px;overflow:hidden;text-overflow:ellipsis; white-space:nowrap;}
.list div a{ color:#5299D3; text-decoration:none}
.list div a:hover{ color:#F00; font-weight:bold}
</style>
</head>
<body>
	<div style="margin:0 auto;width:700px;">
		<div class="info">当前用户：<%if(user!=null){%><%=user.getUname()%><%}%>[<a href="Do/doOut.jsp">登出</a>]</div>
		<div class="main">
			<h1>查询结果</h1>
			<%
				IScoreDao scoreDao=new ScoreDaoImpl();
				Score score=new Score();
				//List<Score> list=new ArrayList<Score>();
				/* System.out.println(user.getId());
				System.out.println(user.getUname()); */
				score=scoreDao.getScore(user.getId());			
			%>
			<div class="list">
				 <div>
					<table>
						<tr>
							<th>Name</th>
							<th>Score</th>
						</tr> 
					</table>
				</div> 
				<div class="list"><div> [<%=score.getSname() %>]:[<%=score.getSscore() %>]</div></div>
			</div>
		</div>
	</div>
</body>
</html>