<%@ page language="java"
	import="com.gy.sams.dao.*,com.gy.sams.dao.impl.*,com.gy.sams.entity.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	int flag=0;
	request.setCharacterEncoding("utf-8");
	String id=request.getParameter("id");
	
	//删除用户
	IScoreDao scoreDao=new ScoreDaoImpl();
	String uid=scoreDao.getScoreUser_id(id);
	if(uid !=null){
		IUserDao userDao=new UserDaoImpl();
		userDao.deleteUser(uid);
		out.print("<script type='text/javascript'>alert('该用户也被删除');location.replace('../tableScore.jsp')</script>");
	}

	flag=scoreDao.deleteScore(id);
	if(flag!=0){
		out.print("<script type='text/javascript'>alert('删除成功');location.replace('../tableScore.jsp')</script>");
	}
	else{
		out.print("<script type='text/javascript'>alert('删除失败');location.replace('../tableScore.jsp')</script>");
	}
%>