package com.gy.sams.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.swing.JDialog;
import javax.swing.JOptionPane;

import com.gy.sams.dao.IScoreDao;
import com.gy.sams.dao.IUserDao;
import com.gy.sams.dao.baseDao;
import com.gy.sams.entity.Score;
import com.gy.sams.entity.User;



public class ScoreDaoImpl extends baseDao implements IScoreDao {
	Connection conn=null;
	PreparedStatement pstmt=null;
	ResultSet rs=null;
	/*
	 查询学生成绩
	 */
	@Override
	public List<Score> getAllScores() throws Exception {
		// TODO Auto-generated method stub
		/*
		 1.定义sql
		 2.连接数据库，执行sql,关闭
		 3.返回查询结果
		 */
		//1.定义sql
		String sql="select * from _score order by sscore desc";
		List<Score> list= new ArrayList<Score>();
		//2.连接数据库，执行sql,关闭
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				Score score=new Score();
				score.setId(rs.getString("id"));
				score.setSname(rs.getString("sname"));
				score.setSscore(rs.getString("sscore"));
				score.setUser_id(rs.getString("user_id"));
				list.add(score);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("查询异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, rs);
		}
		//返回查询结果
		return list;
	}

	/*
	 *删除学生成绩
	 */
	
	@Override
	public int deleteScore(String id) throws Exception {
		// TODO Auto-generated method stub
		/*0.校验参数
		 * 1.定义sql
			2.连接数据库，执行sql,关闭
			 3.返回更新执行次数
		 */
		
		//0.校验参数
		if(id== null) {
			throw new Exception("id不能为空！");
		}
		//1.定义sql
		String sql="delete from _score where _score.id =?";
		//2.连接数据库，执行sql,关闭
		int num=0;
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			num=pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("删除成绩异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, null);
		}
		//3.返回查询执行次数
		return num;
	}

	/*
	 * 增加学生成绩
	 */
	@Override
	public int addScore(String sname,String sscore) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * 0.添加user
		 * 1.写查询语句
		 * 2.连接，执行sql，关闭
		 * 3.返回执行次数
		 */
		//0.添加user
		User user=new User();
		user.setUname(sname);
		user.setUpass(sname);
		user.setRole(0+"");
		IUserDao userDao=new UserDaoImpl();
		user=userDao.addUser(user.getUname(),user.getUpass(),user.getRole());
		String userid=user.getId();
		//1.写查询语句
		String sql="insert into _score values(?,?,?,?)";
		int num=0;
		//2.连接，执行sql，关闭
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			String id = UUID.randomUUID().toString();//id 随机生成varchar id
			pstmt.setString(1, id);
			pstmt.setString(2, sname);
			pstmt.setString(3, sscore);
			pstmt.setString(4, userid);
			pstmt.executeUpdate();
			num++;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("插入方法异常！");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, null);
		}
		//3.返回执行次数
		return num;
	}

	/*
	 *  修改学生成绩
	 */
	@Override
	public int updateScore(String id,String a) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * 0.校验参数
		 * 1.写查询语句
		 * 2.连接，执行sql，关闭
		 * 3.返回执行次数
		 */
		// 0.校验参数
		if(id==null) {
			throw new Exception("id不能为空");
		}
		//1.写查询语句
		String sql="update _score set _score.sscore=? where _score.user_id=?";
		int num=0;
		//2.连接，执行sql，关闭
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, a);
			pstmt.setString(2, id);
			num=pstmt.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("修改学生成绩异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, null);
		}
		//3.返回执行次数
		return num;
	}

	/*
	 *  返回单个学生成绩
	 */
	@Override
	public Score getScore(String id) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * 0.参数校验
		 * 1.写查询语句
		 * 2.执行sql
		 * 3.返回查询结果
		 */
		 //0.参数校验
		if(id==null) {
			throw new Exception("id不能为空!");
		}
		//1.写查询语句
		String sql="select * from _score where _score.user_id=?";
		Score score=null;
		//2.执行sql
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs=pstmt.executeQuery();
			score=new Score();
			while(rs.next()) {
				score.setId(rs.getString("id"));
				score.setSname(rs.getString("sname"));
				score.setSscore(rs.getString("sscore"));
				score.setUser_id(rs.getString("user_id"));
				
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("查询个人成绩异常！");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, rs);
		}
		return score;
	}

	/*
	 * 返回单个学生成绩里的user_id
	 */
	@Override
	public String getScoreUser_id(String id) throws Exception {
		// TODO Auto-generated method stub
		/*
		 * 1.写查询语句
		 * 2.执行sql
		 * 3.返回查询结果
		 */
		//1.写查询语句
		String sql="select * from _score where _score.id=?";
		String uid="";
		//2.执行sql
		try {
			conn=this.getConn();
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs=pstmt.executeQuery();
			while(rs.next()) {
				uid=rs.getString("user_id");
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("返回单个学生成绩里的user_id异常");
			e.printStackTrace();
		}finally {
			this.closeAll(conn, pstmt, rs);
		}
		//3.返回查询结果
		return uid;
	}

}
