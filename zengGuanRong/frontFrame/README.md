# 组内使用的前端框架前手架

## 更新记录 
2017-10-16更新：
- 脚手架初步建立
    - webpack和package.json建立，构建模块形成
    - entry/index.js等模块完成，实现React入口文件
    - test模块建立，构建react组件
    - actions及其后台接口完成调试完成，后端数据可获取

## 建设理由
方便项目组后续使用以及新人学习练习。

## 使用方法
### 运行环境：
1. git项目版本管理软件
2. node.js(本脚手架基于8.5.0版本建立)，确保npm已经安装
3. 编辑器使用vscode

### 项目启动
利用npm下载对应构件包（最好在翻墙情况下下载），在命令行下：  
```npm install```  
确认完成后无报错(warning可以暂时忽略)

启动项目，在命令行下：
```npm start```  
无报错的情况下node自动进行，并且实现了热更新（随着代码修改node会进行实时编译并且展示对应网页）。

常见报错情况：
- 端口已经被占用。解决办法：在`webpack.dev.js`中找到`devServer.port`的端口改成其他端口号，推荐80，8080，8090等。
- 部分情况需要构建多次，继续执行`npm start`。
- 检查`npm install`中有无报错，继续运行。

### 主要组件的使用方法
- `src/entry/index.jsx`为入口文件，即主页的主要内容。
- `src/view/test/index.jsx`为其中一块组件，实现一个表格加一些文字。
- `src/actions/test.js`对应`src/view/test/index.js`的actions文件，同时实现了一个后台接口。
- `src/reducers/test.js`对应为`src/view/test/index.js`的reducers文件，修改`src/view/test/index.js`下的数据流。
- `src/reducers/index.js`将`src/reducers/test.js`的reducer组件合并到根reducers中。
- `src/actions/actionTypes.js`中存放各种reducers命令标签。
- 样式文件建议使用less语法。
- js语法建议使用ES6或以上版本。

可以根据上述文件的形式进行改写、新增等。

### 注意事项
- 需要进行编辑时请将本文件移动到自己的文件夹进行处理，不要污染此处的文件框架。
- 进行修改后记得更新“更新记录”。
- 若使用出现问题，请和群内“文笔超好的男同学”联系。