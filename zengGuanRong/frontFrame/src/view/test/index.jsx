import React from 'react';
import { connect } from 'react-redux';
import * as testAction from '../../actions/test'

import { Table,Modal } from 'antd';

class Test extends React.Component{
    constructor(props){
        super(props);
    }

    state = { visible: false }

    // 弹层窗口设置
    showModal = () => {
      this.setState({
        visible: true,
      });
    }
    handleOk = (e) => {
        // console.log(e);
        this.setState({
          visible: false,
        });
      }
      handleCancel = (e) => {
        // console.log(e);
        this.setState({
          visible: false,
        });
      }
    
    componentDidMount() {
        this.props.dispatch(testAction.list());
    }

    // 列表表头
    tableColumns = [
        { title: '新闻标题', dataIndex: 'title', key: 'title', },
        { title: '新闻链接', dataIndex: 'href', key: 'href', },
        {title:'更新日期',dataIndex:'publishDate',key:'publishDate'},
        {
            title: '操作', key: 'operate', render: (text, record) => (
                <div><a href="#" onClick={event => {this.showModal()}}>点击进入</a>
                    <Modal
                        title="提示框"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        <p>想点开网页？不存在的，就是给你看看而已</p>
                    </Modal>
                </div>
            )
        },
    ]

    render() {
        return (
            <div>
                test组件里面给的内容以及渲染的antd组件<br />
                <Table
                    bordered
                    pagination={{
                        total: this.props.list.length,
                        showTotal: (total, range) => `共 ${total} 条`
                    }}
                    dataSource={this.props.list}
                    columns={this.tableColumns}
                    rowKey={record => record.id}
                />
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const { list} = state.test;
    
    return {
        list,
    }
}

export default connect(mapStateToProps)(Test)