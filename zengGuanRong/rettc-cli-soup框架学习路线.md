# rettc-cli-soup框架学习路线
[TOC]
## 框架简介
框架以react-redux技术栈为基础，从申报系统开始构建，经过多次迭代完善后得到，除了具备完成react-redux基本功能，还包括实时编译更新、代码检查等多项功能，目前尚在完善和改进过程，欢迎大家在开发过程中将遇到的问题反映给数学极客开发团队，联系人谢忠甫，曾冠荣。

### 开发技术栈
- react：用于构建用户界面的JavaScript库，已经并将长期为主流前端界面开发提供解决方案，由于其特性，适合交互比较复杂频繁的web应用。
- redux：Redux是JavaScript 状态容器，提供可预测化的状态管理，解决react中出现的有关数据流的问题。
- react-redux：提供界面构造工具react和redux共同使用的接口工具。
- fetch：提供了一个获取资源的接口（包括跨域），与XMLHttpRequest有非常多的相似之处，而相比AJAX和原生的XMLHttpRequest，具有语法简洁语义化，基于Promise标准实现，支持async/await，易于重构等优点。
- antd：蚂蚁金服提供的旨在统一中后台项目的前端 UI设计，屏蔽不必要的设计差异和实现成本，解放设计和前端的研发资源

## 学习路线
### react
#### 写在前面
react是一门难度较高，深度掌握该库需要有较为深厚的JS基础和经验，所以做好心理准备，但是不用担心，由于现在已经成为了主流，所以无论是文档上，还是教程上，都有十分完善的材料。

#### 入门
入门阶段主要是了解react的主要功能和特性。推荐学习材料是阮一峰老师的[React 入门实例教程](http://www.ruanyifeng.com/blog/2015/03/react.html)，不会像文档一样枯燥，内容比较完全，还有示例程序，所以非常推荐，可以以这份材料为主线开始学习，已经开始学习的也可以参考这份材料。在此基础上，推荐下面几个辅助材料：  

- [一步一步写React示例程序](http://note.youdao.com/noteshare?id=b56d5e3755316e972a911995144f950b&sub=6EDD1F5A365649489FDB1FE948618C3D)：个人学习的笔记，基本来源于官方文档和下面的学习材料
- [极客学院 | 深入理解React](http://wiki.jikexueyuan.com/project/react/thinking-in-react.html)：和阮一峰老师的教程在思想上有很多相似点，但个人感觉这个材料的内容没有阮一峰老师的容易接受。
- [React官方中文文档](http://www.react-cn.com/docs/tutorial.html)：文档里面框架内所有的功能，作为词典进行查阅，可以在学习过程中进行补充。
- [React官方英文文档](https://facebook.github.io/react/)：比中文的更加原汁原味。
- [React的github](https://github.com/facebook/react)：还有什么比代码更原汁原味的吗？看懂代码，别说用了，自己造轮子都没问题。
- 曹天岳同学的[网络三件套VSReact全家桶](https://gitee.com/chashaozgr/mathsNewGeekers/wikis/%E7%BD%91%E7%BB%9C%E4%B8%89%E4%BB%B6%E5%A5%97%20VS%20React%E5%85%A8%E5%AE%B6%E6%A1%B6.md)

#### 进阶
上述阶段完成后，大概就能理解react的官方内容。下面可以在rettc-cli-soup框架下进行实践。首先按照[rettc-cli项目脚手架模块使用方法](http://note.youdao.com/noteshare?id=66354f800be2d2731551ea3dfdacf9ef&sub=7B2051BE4DFF49AB982207F0CCDECD90)进行框架的配置，然后按照里面的[readme.md](https://gitee.com/rettc/rettc-cli-soup/blob/master/README.md)方法进行环境配置和使用，进行一个简单的demo编辑，进行一些简单的例子练习。

#### 实战练习
完成[一步一步写React示例程序](http://note.youdao.com/noteshare?id=b56d5e3755316e972a911995144f950b&sub=6EDD1F5A365649489FDB1FE948618C3D)中的所有内容并进行展示。


### antd
antd是一套基于react实现的UI库，但是却提出不仅仅是UI库的一整套react设计模式和方案，此处不要求太过于深究，能够理解其中的基本内涵和使用方法即可。

#### 学习路线
根据antd的[官网](https://ant.design/index-cn)进行学习，在rettc-cli-soup框架下进行各种组件的使用和练习，掌握其基本使用方法，了解文档查阅方式，能够根据文档实现相应的功能，重点阅读[组件](https://ant.design/docs/react/introduce-cn)内提到的多个components的功能。

#### 实战练习
绘出申报系统中其中一个界面，如下图所示：
![申报系统运营管理](https://note.youdao.com/yws/public/resource/5823e1d943e8e5c33ce67c3cc05a564a/xmlnote/47B0D02BB2F149C3B5DB7F2147573623/46939)
并且完成但是不限于下列功能：  

- 数据表的新增功能
    + 上方填写信息后，点击保存信息可以将信息进行存储，并且自动存在下方的列表中
- 数据表的修改功能
    + 点击下方该条数据的修改操作按钮时，该条数据表的信息能够拉到上方填写栏，点击保存信息（自己再加一个按钮也行）后下方的数据表能够被修改
- 数据表的删除功能
    + 点击下方数据栏任意一条数据的删除按钮，该条数据被删除

### redux与react-redux
redux是一个比较困难的内容，需要花费比较多的时间和精力，难度也比较高，数据在前端进行流通，但是并不能被明显地看到，建议多console将过程数据展示出来，同时结合谷歌浏览器redux DevTools（需要翻墙）协助debug理解。

#### redux学习路线
推荐按照[Redux莞式教程（推荐）](https://github.com/kenberkeley/redux-simple-tutorial)进行学习，有比较完整的资源，跟着做和学习，能比较快捷地理解其中的主要内容，结合[Redux中文文档](http://www.redux.org.cn/)和[Redux英文文档](http://redux.js.org/)进行扩充即可有比较好的理解。

另外可以参考我的[学习笔记](http://note.youdao.com/noteshare?id=44bf77783b14084c867b3aa0e0196c25&sub=1567667342064D118BD3DEE88AD16182)。

#### react-redux学习路线
这是讲两者结合的一个解决方案，内容不多，结合我的[笔记文档](http://note.youdao.com/noteshare?id=e97abeccf1bf596bd64e44112843769a&sub=3ED719BAF4074E86881B004E1E4AEAEC)（非常自信）进行学习即可。

#### 实战练习
完成我的[笔记文档](http://note.youdao.com/noteshare?id=e97abeccf1bf596bd64e44112843769a&sub=3ED719BAF4074E86881B004E1E4AEAEC)中提到的一个todo应用，和官方文档中提到的是一个东西，所以可以参考。

### fetch
fetch是一个非常接近于XMLHttpRequest的资源请求工具，具体可以看[fetch用法说明](https://segmentfault.com/a/1190000007019545)，个人感觉对promise完全没概念的同学可以稍微看一下[MDN中对promise的介绍](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Promise)，然后结合[MDN中的fetch介绍](https://developer.mozilla.org/zh-CN/docs/Web/API/GlobalFetch/fetch)进行学习。

另外可以参考rettc-cli-soup中的示例程序中对fetch的使用以及在框架中的封装，并尝试进行使用。

## 结业实战与后记
到了这步，如果对上面的内容非常熟悉，那么恭喜你，你已经基本完成了学习任务，可以开始进行实战练习，可以找曾冠荣或者谢忠甫要终极任务——制作申报系统中的一个模块页面，其中完整涉及了上述所有内容，如果你能全部实现，说明你已经走在时代前沿，2018年的主流框架之一，有了这套技术栈，能够完成大量高端的工作，也说明你有一定的开发工地，一方面你能完成相对复杂的前端操作，另一方面你也可以可以往框架、工具、前端底层进发，团队同样欢迎你加入框架维护、nodejs编程等工作中。