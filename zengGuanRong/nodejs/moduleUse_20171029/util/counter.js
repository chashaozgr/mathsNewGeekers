'use strict'

// 设置了私有变量i，然后还有一个函数count，导出的是一个共有方法count
var i=0;

function count (){
	return ++i
}

exports.count=count;